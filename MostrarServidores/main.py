from flask import json,jsonify
from ArubaCloud.PyArubaAPI import CloudInterface
from ArubaCloud.objects.VmTypes import Smart

def mostrar_vm(request):
    user = request.args.get('user', '')
    passwd = request.args.get('passwd', '')
    ci = CloudInterface(dc=1)
    ci.login(username=user, password=passwd, load=True)
    vmList = []
    try:
        for vm in ci.vmlist:
                smartVM = {
                    'Nombre': vm.vm_name,
                    'Id': vm.sid,
                    'Status' : vm.status}
                vmList.append(smartVM)
                
    except Exception as e:
        return str(e)

    jsonStr = json.dumps(vmList)
    return jsonStr