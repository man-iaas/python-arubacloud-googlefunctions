from flask import json,jsonify
import os
from ftplib import FTP 

def mostrar_playbooks(request):
    headers = {
        'Access-Control-Allow-Origin': '*'
    }
    user = request.args.get('user', '')
    passwd = request.args.get('passwd', '')
    server_host = request.args.get('server-host' '')
    
    pbList = []
    ftp = FTP()
    ftp.connect(server_host)
    ftp.login(user, passwd)
    ftp.cwd('/var/www/html/playbooks')
    
    try:
        for pb in ftp.nlst():
                pbJson = {
                    'nombre': pb
                    }
                pbList.append(pbJson)
                
    except Exception as e:
        return str(e)
    
    jsonStr = json.dumps(pbList)
    return(jsonStr,headers)