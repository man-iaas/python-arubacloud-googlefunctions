from ArubaCloud.PyArubaAPI import CloudInterface
from ArubaCloud.objects.VmTypes import Smart
import time

def restaurar_vm(request):
    user = request.args.get('user', '')
    passwd = request.args.get('passwd', '')
    server_name = request.args.get('server-name', '')
    new_passwd = request.args.get('new-passwd', '')
    ci = CloudInterface(dc=1)
    ci.login(username=user, password=passwd, load=True)
    for vm in ci.vmlist:
        if vm.vm_name == server_name:
            try:
                ci.poweroff_server(vm,vm.sid)
                time.sleep(20)
                vm.reinitialize(admin_password=new_passwd)
            except Exception as e:
                return str(e)
            return 'True'