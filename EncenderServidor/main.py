from ArubaCloud.PyArubaAPI import CloudInterface
from ArubaCloud.objects.VmTypes import Smart

def encender_servidor(request):
    user = request.args.get('user', '')
    passwd = request.args.get('passwd', '')
    server_name = request.args.get('server-name' '')
    ci = CloudInterface(dc=1)
    ci.login(username=user, password=passwd, load=True)
    for vm in ci.vmlist:
        if vm.vm_name == server_name:
            try:
                ci.poweron_server(vm,vm.sid)
            except Exception as e:
                return str(e)
            return 'True'