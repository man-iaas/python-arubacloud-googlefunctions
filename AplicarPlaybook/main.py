import requests
import os

def aplica_playbook(request):
    headers = {
        'Access-Control-Allow-Origin': '*'
    }
    responsePb = requests.get('http://80.211.56.94/playbooks/ping.yml')
    responseHost = requests.get('http://80.211.56.94/hosts/hosts')
    
    hosts = open("/tmp/hosts", "w+")
    playbook = open("/tmp/playbook.yml", "w+")
    
    try:
        playbook.writelines(responsePb.text)
        playbook.seek(0)
        
        hosts.writelines(responseHost.text)
        hosts.seek(0)
        
        for linea in playbook:
            print(linea.rstrip())
            
        for linea2 in hosts:
            print(linea2.rstrip())
		
        os.system('ansible-playbook playbook.yml')
    
    except Exception as e:
        return str(e)
    
    finally:
        playbook.close()
        hosts.close()
       	os.chdir('/tmp')
        os.remove('hosts')
        os.remove('playbook.yml')
        os.system('ls /tmp')
        return('True', headers)